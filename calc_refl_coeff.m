function [coeff_h coeff_v] = calc_refl_coeff(sigma, f, kappa, psi)
%    global epsilon_0;
    epsilon_0 = 8.8541878176e-12;
    
    omega = 2 * pi() * f;
    chi = sigma / (omega * epsilon_0);
    
%vertical    
    nominator = (kappa - 1i * chi) * sin(psi) - sqrt((kappa - 1i * chi) - (cos(psi))^2);
    denominator = (kappa - 1i * chi) * sin(psi) + sqrt((kappa - 1i * chi) - (cos(psi))^2);
    
    coeff_v = nominator / denominator;
    
%horizontal    
    nominator = sin(psi) - sqrt((kappa - 1i * chi) - (cos(psi))^2);
    denominator = sin(psi) + sqrt((kappa - 1i * chi) - (cos(psi))^2);
    
    coeff_h = nominator / denominator;
end
