
R = (29*12 + 8)*0.0254;
Step_deg = 30;

Fs = 1000000;
N = 5000;
Fc = 431000000;
F_IF = 20000;

common_height = 1;

%all angles in rad!!
R_Ant = 10*0.0254/sqrt(3);
Antennas = struct('offset_time', {0, 0.00015, 0.0003}, ...
                    'on_time', {0.000150, 0.00015, 0.00015}, ...
                    'off_time', {0.0003, 0.0003, 0.0003}, ...
                    'pos', {[R_Ant*cos(0) R_Ant*sin(0) common_height], [R_Ant*cos(-2/3*pi()) R_Ant*sin(-2/3*pi()) common_height], [R_Ant*cos(2/3*pi()) R_Ant*sin(2/3*pi()) common_height]}, ...
                    'ro', {1, 1, 1}, ...
                    'phi', {0, 0, 0}, ...
                    'refl_ampl', {0, 0, 0}, ...
                    'refl_phase', {70/180*pi(), 70/180*pi(), 70/180*pi()});
%                    'refl_ampl', {0.1, 0.1, 0.1}, ...
%                    'refl_phase', {70, 70, 70});   
          
Antenna_num = size(Antennas, 2);
Antenna_switch_period_time = sum([Antennas.on_time]);
shall_plot = 0;

% Planes = struct('point', {[0 0 0], [-11 0 0]}, ...
%                 'n', {[0 0 1], [1 0 0]}, ... 
%                 'sigma', {0.01, 0.01}, ...
%                 'kappa', {15, 15});
%Planes = struct('point', {[0 0 0]}, ...
%                'n', {[0 0 1]}, ... 
%                'sigma', {0.01}, ...
%                'kappa', {15});
Planes = [];

Step_deg = Step_deg/180*pi();
pos_x = R * cos([0:Step_deg:2*pi()]);
pos_y = R * sin([0:Step_deg:2*pi()]);
pos_z = ones(size(pos_x))*common_height;
pos = [pos_x' pos_y' pos_z'];

%Generate simulated data
addpath('./AntennaSimulation/');
fprintf('\tGenerating simulated data... ');
y = phase_sim5(pos, Fs, N, Fc, F_IF, Antennas, Planes);
disp('Done');

%Evaluate simulated data...
