function [y_out] = phase_sim5(RX_pos, Fs, N, F_carrier, F_IF, Antennas, Planes)
% antenna coupling
% constant phase shifts for channels
% reflecting surfaces (reflection grazing angle dependent)
% initial polarization effects


    %Time, refl_ampl, refl_phase, x, y, R, fi
%     if (size(Antennas,2) ~= 7)
%         disp('Need TX antenna coordinates: Time, x, y, R, fi');
%         return;
%     end

    y_out = [];
    
    if (size(RX_pos,2) ~= 3)
        disp('Need RX antenna coordinates: x, y, z');
        return;
    end
    
    global c;
    c = 299792458;
    
    global Fc;
    Fc = F_carrier;
    
    global lambda;
    lambda = c / Fc;

    global A;
	A = 1;  
    
%    beta = 2 * pi() / lambda;
    Ts = 1/Fs;
    
    y_out = zeros(size(RX_pos,1),N);
    for jj = 1:size(RX_pos,1)
        fprintf('\tGenerating simulated data for RX ant position %d...', jj);        
        
        %Generate mirrorred antennas
        virtual_Antennas = Antennas(1);
        virtual_Antennas.plane_list = [];
        virtual_Antennas(end) = [];        
        for ii = 1:size(Antennas, 2)
            new_virt_ant = recursive_get_mirrored(Antennas(ii), Planes);
            virtual_Antennas = [virtual_Antennas new_virt_ant];
        end
        
        %Generate samples
        t = 0;
        for ii = 1:N
            if ~mod(ii,1000)
                disp(ii);
            end

            y_out(jj,ii) = recursive(RX_pos(jj, :), Antennas, virtual_Antennas, Planes, t, A/1000);
            y_out(jj,ii) = y_out(jj,ii) * exp(1i * 2 * pi() * (F_IF - Fc) * t);
            
            t = t + Ts;
        end
        
        disp('Done');
    end
end


function [y] = recursive(pos, real_Antennas, virtual_Antennas, planes, t, min_req_amp)
%purpose of this function is to calculate the signal that is measured at a
%specific location (specified in pos) in an environment full of antennas (specified in
%Antennas)
    global A;
    global c;
    global lambda;
    global Fc;
    
    if nargin < 6 || isempty(min_req_amp) || min_req_amp <= 0
        min_req_amp = A / 1000;
    end

    y = 0;
     
%At this particular point in space we need to measure an amplitude of at
%least min_req_amp, to have an effect on the simulation. But! At most
%antenna number * A amplitude can be generated by the system! So if the
%required minimum is more than what the system can generate, we know, we
%can just stop the recursive algorithm at this point.
    if min_req_amp > (size(real_Antennas, 2) + size(virtual_Antennas, 2))* A
        return;
    end

    real_Antennas(1).plane_list = [];    
    Antennas = real_Antennas;
    
    if ~isempty(virtual_Antennas)
        visible_virtual_Antennas = virtual_Antennas(1);
        visible_virtual_Antennas(end) = [];
        for ant_counter = 1:size(virtual_Antennas, 2)
            [validity, cum_coeff] = reflection_valid_at_pos(virtual_Antennas(ant_counter), pos, planes);
            if validity   
                visible_virtual_Antennas(end+1) = virtual_Antennas(ant_counter);

                visible_virtual_Antennas(end).ro = visible_virtual_Antennas(end).ro * abs(cum_coeff);
                visible_virtual_Antennas(end).phi = visible_virtual_Antennas(end).phi + angle(cum_coeff);
            end
        end

        Antennas = [real_Antennas visible_virtual_Antennas];
    end
    
    for ant_counter = 1:size(Antennas, 2)
        dist = vec_length(pos - Antennas(ant_counter).pos);        

        %1. Signal generation AND constant phase shift
        t_ant = t - dist/c;
        ampl = 1 / (1 + dist/lambda);
        
        if antenna_transmitting(Antennas(ant_counter), t)
            if ~(min_req_amp > size(Antennas, 2) * A)
                phi = Antennas(ant_counter).phi;
                ro = Antennas(ant_counter).ro;
                y = y + ro * ampl * A * exp(1i * 2 * pi() * Fc * t_ant) * exp(-1i * phi);
            end
        end
        
        %2. Antenna coupling
        t_ant = t_ant - Antennas(ant_counter).refl_phase/360 / Fc;
        ampl = ampl * Antennas(ant_counter).refl_ampl;
        
        if ampl ~= 0
            new_min_req_amp = min_req_amp / ampl;
            if new_min_req_amp <= size(Antennas, 2) * A
                y = y + ampl * recursive(Get_ant_pos(Antennas(ant_counter)), real_Antennas, virtual_Antennas, planes, t_ant, new_min_req_amp);
            end
        end
    end
end


function virtual_antennas = recursive_get_mirrored(Antenna, planes, min_req_A, plane_list)
    global A;
    
    if nargin < 4
        plane_list = [];
    end
    if (nargin < 3) || isempty(min_req_A)
        min_req_A = A / 1000;
    end

    Antenna.plane_list = [];
    virtual_antennas = Antenna;
    virtual_antennas(end) = [];
    for ii=1:length(planes)
        plane_point = planes(ii).point;
        n = planes(ii).n;
        
        new_min_req_A = min_req_A / 0.999; %HACK magical constant

        new_plane_list = [plane_list ii];
        
        try
            new_pos = mirror_point_on_plane(Antenna.pos, n, plane_point);
            
            virtual_antennas(end+1) = Antenna;
            virtual_antennas(end).pos = new_pos;
            virtual_antennas(end).plane_list = new_plane_list;
            
            if min_req_A < A
                new_virt_ant = recursive_get_mirrored(virtual_antennas(end), planes, new_min_req_A, new_plane_list);
                if ~isempty(new_virt_ant);
                    virtual_antennas = [virtual_antennas new_virt_ant];
                end
            end
        catch errRecord
            if strcmp(errRecord.identifier, 'VerifyOutput:WrongSideOfPlane')
                %obviously antenna is on the worng side of the reflecting
                %surface
            end
        end        
    end
end


function [validity, cum_coeff] = reflection_valid_at_pos(virtual_antenna, pos, planes)
%WARNING, TODO E field along z axis only both at receiver and transmitter
    global Fc;

    validity = 0;
    cum_coeff = 1;
    direction = virtual_antenna.pos - pos;
    plane_list = virtual_antenna.plane_list;
    
    E = [0 0 1];

    while ~isempty(plane_list) 
        pos_candidate = [];
        direction_candidate = [];
        n_candidate = [];
        plane_ID = [];
        for ii = 1:length(planes)
            n = planes(ii).n;
            plane_point = planes(ii).point;
            
            try
                [new_pos, new_direction] = ray_plane_intersection(pos, direction, n, plane_point);
                                
                if ~isempty(pos_candidate)
                    if vec_length(pos_candidate - pos) <= vec_length(new_pos - pos)
                        continue;
                    end 
                end
                pos_candidate = new_pos;
                direction_candidate = new_direction;
                plane_ID = ii;
            catch errRecord
                %no intersection
            end
        end
        if plane_ID ~= plane_list(1)
            return;
        end
        plane_list(1) = [];
        
        n = planes(plane_ID).n;
        sigma = planes(plane_ID).sigma;
        kappa = planes(plane_ID).kappa;
        
        cos_psi_plus_halfpi = (direction * n')/(vec_length(direction)*vec_length(n));
        psi = acos(cos_psi_plus_halfpi) - pi()/2;
        [coeff_h coeff_v] = calc_refl_coeff(sigma, Fc, kappa, psi);

        [H V] = xyz2HV(E, direction, n);
        H = H * coeff_h;
        V = V * coeff_v;
        E = HV2xyz(H, V, direction_candidate, n);        
            
        pos = pos_candidate;
        direction = direction_candidate;
    end
    cum_coeff = E(3);   
    validity = 1;
end


function new_pos = mirror_point_on_plane(pos, n, plane_point)
    n = n / vec_length(n); %just in case...
    length_from_point_to_plane = (plane_point - pos) * n';
    if length_from_point_to_plane > 0
        errRecord = MException('VerifyOutput:WrongSideOfPlane', ...
            'Point is on the wrong (non reflecting) side of the plane');
        throw(errRecord);
    end
    moving_vec = 2 * length_from_point_to_plane * n;
    new_pos = pos + moving_vec;
end


function [intersection, new_direction] = ray_plane_intersection(pos, direction, n, plane_point)
    n = n / vec_length(n); %just in case...
    length_from_point_to_plane = (plane_point - pos) * n';
    if length_from_point_to_plane > 0
        errRecord = MException('VerifyOutput:WrongSideOfPlane', ...
            'Point is on the wrong (non reflecting) side of the plane');
        throw(errRecord);
    end
    projection_on_normal_vec = direction * n';
    if projection_on_normal_vec > 0
        errRecord = MException('VerifyOutput:WrongRayDirection', ...
            'Ray direction is not towards the the plane');
        throw(errRecord);
    end
    
    intersection = pos + direction / projection_on_normal_vec * length_from_point_to_plane;
    new_direction = direction - 2 * projection_on_normal_vec * n;
end


function length = vec_length(pos)
    length = sqrt(pos * pos');
end


function transmitting = antenna_transmitting(Antenna, t)
    if mod(t - Antenna.offset_time, Antenna.on_time + Antenna.off_time) <= Antenna.on_time
        transmitting = 1;
    else
        transmitting = 0;
    end
end


function [H V] = xyz2HV(E, direction, n)
    H_unit = cross(n, direction);
    H_unit = H_unit / vec_length(H_unit);
    V_unit = cross(-H_unit, direction);
    V_unit = V_unit / vec_length(V_unit);
    H = dot(E, H_unit);
    V = dot(E, V_unit);
end


function [E] = HV2xyz(H, V, direction, n)
    H_unit = cross(n, direction);
    H_unit = H_unit / vec_length(H_unit);
    V_unit = cross(-H_unit, direction);
    V_unit = V_unit / vec_length(V_unit);
    E = H * H_unit + V * V_unit;
end