function [y_out] = phase_sim(RX_pos, Fs, N, Fc, F_IF, Antennas)
    %Time, x, y, R, fi
    if (size(Antennas,2) ~= 5)
        disp('Need TX antenna coordinates: Time, x, y, R, fi');
        return;
    end
    
    if (size(RX_pos,2) ~= 2)
        disp('Need RX antenna coordinates: x, y');
        return;
    end
    
    c = 299792458;
    beta = 2 * pi() * Fc / c;
    Ts = 1/Fs;
    
    y_out = zeros(size(RX_pos,1),N);
    for jj=1:size(RX_pos,1)
        fprintf('\tGenerating simulated data for RX ant position %d...', jj);        
        
        t = 0;
        rel_t = 0;
        ant_counter = 1;
        for ii=1:N
            if ~mod(ii,100000)
                disp(ii);
            end
            
            while (rel_t >= Antennas(ant_counter, 1))
                rel_t = rel_t - Antennas(ant_counter, 1);
                ant_counter = mod(ant_counter + 1 - 1, size(Antennas,1)) + 1;
            end

            if sum(isnan(Antennas(ant_counter,:)))
                ant_counter_prev =  mod(ant_counter - 1 - 1, size(Antennas,1)) + 1;
                ant_counter_next = mod(ant_counter + 1 - 1, size(Antennas,1)) + 1;

                t1 = Antennas(ant_counter, 1);

                x1 = Antennas(ant_counter_prev, 2);
                y1 = Antennas(ant_counter_prev, 3);
                R1 = Antennas(ant_counter_prev, 4);
                fi1 = Antennas(ant_counter_prev, 5);

                x2 = Antennas(ant_counter_next, 2);
                y2 = Antennas(ant_counter_next, 3);
                R2 = Antennas(ant_counter_next, 4);
                fi2 = Antennas(ant_counter_next, 5);

                x = x1 + (rel_t / t1) * (x2 - x1);
                y = y1 + (rel_t / t1) * (y2 - y1);
                R = R1 + (rel_t / t1) * (R2 - R1);

                delta_fi(1) = fi2 - fi1;
                delta_fi(2) = delta_fi(1) - sign(delta_fi(1))*2*pi();
                [val, pos] = min(abs(delta_fi));
                fi = fi1 + (rel_t / t1) * delta_fi(pos);
            else
                x = Antennas(ant_counter, 2);
                y = Antennas(ant_counter, 3);
                R = Antennas(ant_counter, 4);
                fi = Antennas(ant_counter, 5);
            end

            x = x + R * cos(fi);
            y = y + R * sin(fi);
            dist = sqrt((RX_pos(jj,1) - x).^2 + (RX_pos(jj,2) - y).^2);

            y_out(jj,ii) = cos(2*pi()*F_IF*t - beta*dist) + 1i * sin(2*pi()*F_IF*t - beta*dist);
            t = t + Ts;
            rel_t = rel_t + Ts;
        end
        
        disp('Done');
    end
end