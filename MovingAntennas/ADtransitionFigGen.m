R = 10;
fi = 27;

Fs = 1000000;
Ts = 1/Fs;
N = 1000;
Fc = 401000000;
F_IF = 9000;

T_full = 0.001;

fi = fi/180*pi();
pos_x = R * cos(fi);
pos_y = R * sin(fi);
pos = [pos_x' pos_y'];

y_arch = [];
y_out_arch = [];
for T_stat=0:0.00009:T_full/3
    T_mov = T_full/3 - T_stat;
    N = T_full*Fs;
    Antennas = [T_stat, 0, 0, 0.35, pi()/3; ...
            T_mov, NaN, NaN, NaN, NaN; ... 
            T_stat, 0, 0, 0.35, pi(); ...
            T_mov, NaN, NaN, NaN, NaN; ... 
            T_stat, 0, 0, 0.35, -pi()/3; ...
            T_mov, NaN, NaN, NaN, NaN];

    %Generate simulated data
    fprintf('\tGenerating simulated data... ');
    y = phase_sim(pos, Fs, N, Fc, F_IF, Antennas);
    disp('Done');

    y_u=unwrap(angle(y));
    y_du=y_u(2:end)-y_u(1:end-1);
    y_mean = y_du - mean(y_du);
    y_out = zeros(1, length(y_mean));
    y_out(1) = y_mean(1);
    for ii = 2:length(y_mean)
        y_out(ii) = y_out(ii-1) + y_mean(ii);
        if (mod(ii,5000) == 0)
            disp(ii);
        end
    end
    y_out = y_out - mean(y_out);
    y_out = y_out/pi()*180;


    y_arch(end + 1,:) = y;
    y_out_arch(end + 1,:) = y_out;
    
    figure;
    subplot(2,1,1)
%    plot([real(y)' imag(y)']);
    plot(Ts*([1:length(y)]-1),real(y));
    title('Doppler signal in time domain');
    xlabel('time [s]');
    ylabel('signal');
    subplot(2,1,2)
    plot(Ts*([1:length(y_out)]-1),y_out);
    title('Phase of Doppler signal');
    xlabel('time [s]');
    ylabel('Phase [�]');
end

    figure;
    subplot(2,1,1)
%    plot([real(y)' imag(y)']);
    plot((Ts*([1:length(y)]-1))',real(y_arch)');
    title('Doppler signal in time domain');
    xlabel('time [s]');
    ylabel('signal');
    subplot(2,1,2)
    plot((Ts*([1:length(y_out_arch)]-1))',y_out_arch');
    title('Phase of Doppler signal');
    xlabel('time [s]');
    ylabel('Phase [�]');
