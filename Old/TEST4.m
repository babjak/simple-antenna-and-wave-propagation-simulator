
R = 10;
Step_deg = 40;

Fs = 1000000;
N = 5000;
Fc = 401000000;
F_IF = 20000;

common_height = 0.1;

Antennas = struct('offset_time', {0, 0.00015, 0.0003}, ...
                    'on_time', {0.000150, 0.00015, 0.00015}, ...
                    'off_time', {0.0003, 0.0003, 0.0003}, ...
                    'pos', {[0.1*cos(pi()/3) 0.1*sin(pi()/3) common_height],[0.1*cos(pi()) 0.1*sin(pi()) common_height],[0.1*cos(-pi()/3) 0.1*sin(-pi()/3) common_height]}, ...
                    'ro', {1, 1, 1}, ...
                    'phi', {0, 0, 0}, ...
                    'refl_ampl', {0, 0, 0}, ...
                    'refl_phase', {70, 70, 70});
%                    'refl_ampl', {0.1, 0.1, 0.1}, ...
%                    'refl_phase', {70, 70, 70});   
          
Antenna_num = size(Antennas, 2);
Antenna_switch_period_time = sum([Antennas.on_time]);
shall_plot = 0;

Planes = struct('point', {[0 0 0], [-11 0 0]}, ...
                'n', {[0 0 1], [1 0 0]}, ... 
                'ro', {0.9, 0.9}, ...
                'phi', {70, 70});

Step_deg = Step_deg/180*pi();
pos_x = R * cos([0:Step_deg:2*pi()]);
pos_y = R * sin([0:Step_deg:2*pi()]);
pos_z = ones(size(pos_x))*common_height;
pos = [pos_x' pos_y' pos_z'];

%Generate simulated data
fprintf('\tGenerating simulated data... ');
y = phase_sim4(pos, Fs, N, Fc, F_IF, Antennas, Planes);
disp('Done');

%Evaluate simulated data
y_in = {};
for ii=1:size(y,1)
    y_in{end + 1} = phase_eval_th7(y(ii, :), Fs, Antenna_switch_period_time, Antenna_num, shall_plot);
end
phase_eval_th_complete4(y_in);
