
R = 10;
Step_deg = 20;

Fs = 1000000;
N = 50000;
Fc = 401000000;
F_IF = 20000;
Antennas = [0.000150, 0, 0, 0.1, pi()/3; ... 
            0.000150, 0, 0, 0.1, pi(); ...
            0.000150, 0, 0, 0.1, -pi()/3];
Antenna_num = size(Antennas, 1);
Antenna_switch_period_time = sum(Antennas(:,1));
shall_plot = 0;


Step_deg = Step_deg/180*pi();
pos_x = R * cos([0:Step_deg:2*pi()]);
pos_y = R * sin([0:Step_deg:2*pi()]);
pos = [pos_x' pos_y'];

%Generate simulated data
fprintf('\tGenerating simulated data... ');
y = phase_sim(pos, Fs, N, Fc, F_IF, Antennas);
disp('Done');

%Evaluate simulated data
y_in = {};
for ii=1:size(y,1)
    y_in{end + 1} = phase_eval_th7(y(ii, :), Fs, Antenna_switch_period_time, Antenna_num, shall_plot);
end
phase_eval_th_complete4(y_in);
