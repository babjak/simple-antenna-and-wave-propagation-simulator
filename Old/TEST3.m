
R = 10;
Step_deg = 40;

Fs = 1000000;
N = 5000;
Fc = 401000000;
F_IF = 20000;

Antennas = struct('time', {0.000150, 0.000150, 0.000150}, ...
                    'pos', {[0 0],[0 0],[0 0]}, ...
                    'R', {0.1, 0.1, 0.1}, ...
                    'fi', {pi()/3, pi(), -pi()/3}, ...
                    'switch_phase', {170, 90, 0}, ...
                    'refl_ampl', {0, 0, 0}, ...
                    'refl_phase', {70, 70, 70});
%                    'refl_ampl', {0.1, 0.1, 0.1}, ...
%                    'refl_phase', {70, 70, 70});   
          
Antenna_num = size(Antennas, 2);
Antenna_switch_period_time = sum([Antennas.time]);
shall_plot = 0;


Step_deg = Step_deg/180*pi();
pos_x = R * cos([0:Step_deg:2*pi()]);
pos_y = R * sin([0:Step_deg:2*pi()]);
pos = [pos_x' pos_y'];

%Generate simulated data
fprintf('\tGenerating simulated data... ');
y = phase_sim3(pos, Fs, N, Fc, F_IF, Antennas);
disp('Done');

%Evaluate simulated data
y_in = {};
for ii=1:size(y,1)
    y_in{end + 1} = phase_eval_th7(y(ii, :), Fs, Antenna_switch_period_time, Antenna_num, shall_plot);
end
phase_eval_th_complete4(y_in);
